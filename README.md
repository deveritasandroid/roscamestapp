# README #

##Sesión 3 - 26/01/16

* Agregado: Validación simple de campos en LoginActivity.java
* Agregado: AsyncTask para llamada a API Rest de Login (LoginTask)
* Agregado: Lógica de login dentro de doInBackground()
* Agregado: Método showAlert() para mostrar ventanas de alerta de acuerdo a los diferentes códigos de estado
* Agregado: Permisos de internet en archivo Manifest

##Sesión 2 - 14/01/16

* Agregado: Propiedad android:textColor a los campos de texto
* Agregado: Propiedad android:inputType="textPassword" para el campo de Password
* Agregado: Clase LoginActivity.java para lógica de Login
+ Agregado: Dependencias Gradle para llamar al servicio web
    * compile 'com.google.code.gson:gson:2.4'
    * compile 'com.squareup.retrofit:retrofit:2.0.0-beta2'
    * compile 'com.squareup.retrofit:converter-gson:2.0.0-beta2'
    * compile 'com.squareup.okhttp:okhttp:2.7.0'
    * compile 'cn.pedant.sweetalert:library:1.3'
+ Modificado: Manifest.xml
    * Agregado: namespace tools xmlns:tools="http://schemas.android.com/tools"
    * Agregado: "replace" para corregir error al construir el proyecto: tools:replace="android:icon,android:theme" 
+ Agregado: Crear 2 paquetes extra llamados entidades y retrofit
    * Agregado: Clase StatusCode con atributos int code y String errorMessage, requiere getters, setters y toString
    * Agregado: Clase UserCredentials con atributos String username y String password, requiere getters, setters y toString.

+ En el paquete retrofit agregar una interface IEvaluador para las llamadas al servicio REST.
    * Agregado: @POST("/evaluar/login") Call<StatusCode> login(UserCredentials cred)
* En LoginActivity obtener referencias del Layout (con el método findViewById) y establecer listener para el botón

##Sesión 1

* Creación del Proyecto con Android Studio
* Agregado: Layout(login_layout.xml) para el formulario de Login
* Configuración: Pendiente