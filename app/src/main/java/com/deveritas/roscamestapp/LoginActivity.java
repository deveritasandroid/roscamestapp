package com.deveritas.roscamestapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.deveritas.roscamestapp.entidades.StatusCode;
import com.deveritas.roscamestapp.entidades.UserCredentials;
import com.deveritas.roscamestapp.retrofit.IEvaluador;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Call;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginActivity extends Activity implements View.OnClickListener {

    private EditText userET, passET;
    private Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        userET = (EditText) findViewById(R.id.usuarioET);
        passET = (EditText) findViewById(R.id.passwordET);

        loginBtn = (Button) findViewById(R.id.loginButton);
        loginBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton:
                String userValue = userET.getText().toString();
                String passValue = passET.getText().toString();
                if(!userValue.equals("") && !passValue.equals("")) {
                    UserCredentials uCreds = new UserCredentials(userValue, passValue);
                    new LoginTask().execute(uCreds);
                } else {
                    // codigo -1 es para campos vacios
                    showAlert(-1);
                }
                break;
        }
    }

    private static String BASE_URL = "http://{192.168.2.163}:10000/";

    // Param 1 - Entrada
    // Param 2 - Avance de proceso
    // Param 3 - Tipo de retorno de metodo interno
    private class LoginTask extends AsyncTask<UserCredentials, Void, StatusCode> {

        @Override
        protected StatusCode doInBackground(UserCredentials... params) {
            // Instanciar retrofit
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            // Instanciar servicio
            IEvaluador servicio = retrofit.create(IEvaluador.class);
            // Obtener parametros
            UserCredentials temp = params[0];
            // Hacer llamada al login
            Call<StatusCode> loginReq = servicio.login(temp);
            Response<StatusCode> loginResp = null;
            // Try catch para manejar exception E/S
            try {
                // Ejecutar peticion
                loginResp = loginReq.execute();
                // Verificar el estado de la petición
                if (loginResp.isSuccess()) {
                    StatusCode status = loginResp.body();
                    return status;
                }
            } catch (IOException e) {
                Log.e("Roscamestapp", "Ocurrio un error E/S: " + e.getMessage());
            }
            return new StatusCode();
        }

        @Override
        protected void onPostExecute(StatusCode statusCode) {
            if (statusCode.getCode()  == 1) {
                // login valido
                showAlert(1);
            } else {
                // login invalidado por servidor
                showAlert(-2);
            }
        }
    }

    private void showAlert(int code) {
        SweetAlertDialog sad = null;
        switch (code) {
            case -1:  // campos vacios
                sad = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE);
                sad.setTitleText("Campos vacios").setContentText("Ambos campos no pueden estar vácios");
                break;
            case -2: // login invalido
                sad = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
                sad.setTitleText("Login Inválido").setContentText("El servidor te dejo en visto");
                break;
            case 1: // login correcto
                sad = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE);
                sad.setTitleText("Login válido").setContentText("Todo OK");
                break;
        }
        sad.show();
    }
}
