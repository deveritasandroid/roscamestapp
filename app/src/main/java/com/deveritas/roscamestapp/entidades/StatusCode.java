package com.deveritas.roscamestapp.entidades;

public class StatusCode {

    private int code;
    private String errorMessage;

    public StatusCode() {
    }

    public StatusCode(int code, String errorMessage) {
        this.code = code;
        this.errorMessage = errorMessage;
    }



    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "StatusCode{" +
                "code=" + code +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
