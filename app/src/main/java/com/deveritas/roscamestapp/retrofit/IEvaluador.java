package com.deveritas.roscamestapp.retrofit;

import com.deveritas.roscamestapp.entidades.StatusCode;
import com.deveritas.roscamestapp.entidades.UserCredentials;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;


public interface IEvaluador {

    @POST("/evaluar/login")
    Call<StatusCode> login(@Body UserCredentials cred);

}
